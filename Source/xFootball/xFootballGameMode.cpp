// Copyright Epic Games, Inc. All Rights Reserved.

#include "xFootballGameMode.h"
#include "xFootballCharacter.h"
#include "UObject/ConstructorHelpers.h"

AxFootballGameMode::AxFootballGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
