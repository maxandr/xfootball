#include "BallHandlerComponent.h"

#include "EnhancedInputComponent.h"
#include "MainBall.h"
#include "GameFramework/Actor.h"
#include "../GAS/BasicAttributeSet.h"
#include "xFootball/xFootballCharacter.h"

UBallHandlerComponent::UBallHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	ControlledBall = nullptr;
	BallOffset = FVector(100.f, 0.f, 0.f); // Смещение мяча относительно игрока
	DribbleRadius = 50.f; // Радиус, в котором мяч может перемещаться вокруг игрока
}

void UBallHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UBallHandlerComponent::PlayerInput(FVector InVector)
{
	if(ControlledBall)
	{
		
	}
}

void UBallHandlerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UBallHandlerComponent::SetBall(AMainBall* NewBall, AxFootballCharacter* ControlledCharacter)
{
	AActor* Owner = GetOwner();
	FVector OwnerLocation = Owner->GetActorLocation();
	FVector OwnerForward = Owner->GetActorForwardVector();
	
	ControlledBall = NewBall;
	float DribblingDistance =  ControlledCharacter->BasicAttributeSet->GetDribblingDistance();
	
	ControlledBall->ApplyForce(FVector(ControlledCharacter->CurrentInput.Y,ControlledCharacter->CurrentInput.X,0)*DribblingDistance);
	ControlledCharacter->SetPlayerGameState(EPlayerState::ChasingBall);
}

void UBallHandlerComponent::ReleaseBall()
{
	ControlledBall = nullptr;
}
