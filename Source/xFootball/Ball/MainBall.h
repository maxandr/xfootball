#pragma once
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "MainBall.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBall, Log, All);
UCLASS()
class AMainBall: public AActor,public IAbilitySystemInterface
{
	GENERATED_BODY()
public:
	AMainBall();
	UFUNCTION(BlueprintCallable)
	void ApplyForce(const FVector& Force);
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
	
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category="Ball")
	TObjectPtr<UStaticMeshComponent> BallMesh;
#pragma region GAS
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override
	{
		return AbilitySystemComponent;
	};
	
	UPROPERTY(BlueprintReadOnly,EditAnywhere,Category="GAS",meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="GAS",meta=(AllowPrivateAccess = true))
	const class UBallAttributeSet* BallAttributeSet;
#pragma endregion

private:
	void HandleMeshCollisions(const float& InDeltaTime);
	static FVector ReflectVector(const FVector& InVector, const FVector& Normal);
	void UpdateRotation(float DeltaTime);

	UPROPERTY(EditAnywhere, Category = "Ball Properties")
	float DampingCoefficient=0.95f;
	UPROPERTY(EditAnywhere, Category = "Ball Properties")
	float MaxBallSpeed=5000.f;

	
	UPROPERTY(EditAnywhere, Category = "Ball Properties")
	float Bounciness=0.7f;
	
	UPROPERTY(EditAnywhere, Category = "Ball Properties")
	FVector AngularVelocity;

	FVector Velocity;
	const float Gravity = 981.0f;
	float SubstepThresholdSpeed=0.0f;
	TArray<UStaticMeshComponent*> MeshComponents;
	int MaxSubsteps=1;
	
};
