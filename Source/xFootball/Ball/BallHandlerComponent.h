#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BallHandlerComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBallHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBallHandlerComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetBall(class AMainBall* NewBall, class AxFootballCharacter* ControlledCharacter);
	void ReleaseBall();

	friend AxFootballCharacter;

protected:
	virtual void BeginPlay() override;
	virtual void PlayerInput(FVector InVector);

	UPROPERTY(EditAnywhere)
	float DribbleRadius;

	UPROPERTY(EditAnywhere)
	FVector BallOffset;
private:
	AMainBall* ControlledBall;
};