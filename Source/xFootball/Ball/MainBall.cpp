#include "MainBall.h"
#include "CoreMinimal.h"
#include "../GAS/BallAttributeSet.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY(LogBall);

AMainBall::AMainBall()
{
	PrimaryActorTick.bCanEverTick = true;
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMeshComponent"));
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	RootComponent = BallMesh;

	AngularVelocity = FVector::ZeroVector;

	SubstepThresholdSpeed = 2000.0f;
	MaxSubsteps = 10; 
}
void AMainBall::BeginPlay()
{
	Super::BeginPlay();
	if(IsValid(AbilitySystemComponent))
	{
		BallAttributeSet = AbilitySystemComponent->GetSet<UBallAttributeSet>();
	}
	
	TArray<AActor*> AllActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AActor::StaticClass(), AllActors);

	for (AActor* Actor : AllActors)
	{
		if(Actor!=this)
		{
			TArray<UStaticMeshComponent*> Components;
			Actor->GetComponents<UStaticMeshComponent>(Components);
			for (UStaticMeshComponent* Component : Components)
			{
				if (Component != BallMesh) // Исключаем мяч
				{
					MeshComponents.Add(Component);
				}
			}
		}
	}
}
void AMainBall::Tick(float DeltaSeconds) 
{
	Super::Tick(DeltaSeconds);
	Velocity.Z -= Gravity * DeltaSeconds;
	
	Velocity *= DampingCoefficient;
	
	int32 NumSteps = 1;
	if (Velocity.Size() > SubstepThresholdSpeed)
	{
		NumSteps = FMath::Clamp(FMath::RoundToInt(Velocity.Size() / SubstepThresholdSpeed), 1, MaxSubsteps);
	}
	
	const float SubstepDeltaTime = DeltaSeconds / NumSteps;
	UE_LOG(LogBall,Log,TEXT("Velocity: %s"), *Velocity.ToString());
	for (int32 i = 0; i < NumSteps; ++i)
	{
		HandleMeshCollisions(SubstepDeltaTime);
	}
	UpdateRotation(DeltaSeconds);
}

void AMainBall::ApplyForce(const FVector& Force)
{
	Velocity += Force;
}

void AMainBall::HandleMeshCollisions(const float& InDeltaTime)
{
	FVector Start = GetActorLocation();
	FVector End = Start + (Velocity * InDeltaTime);
	FHitResult HitResult;

	bool bHit = GetWorld()->SweepSingleByChannel(
		HitResult,
		Start,
		End,
		FQuat::Identity,
		ECC_Visibility,
		FCollisionShape::MakeSphere(BallMesh->Bounds.SphereRadius),
		FCollisionQueryParams(FName(TEXT("Sweep")), false, this)
	);

	if (bHit)
	{
		FVector Normal = HitResult.Normal;
		Velocity = ReflectVector(Velocity, Normal)*Bounciness;

		FVector TangentialVelocity = FVector::CrossProduct(Velocity, Normal);
		AngularVelocity += TangentialVelocity * 0.1f; // Коэффициент 0.1 можно настроить для баланса
		
		FVector NewLocation = HitResult.Location + (Normal * 0.1f);
		SetActorLocation(NewLocation);
	}
	else
	{
		SetActorLocation(End);
	}

	// DrawDebugLine(GetWorld(), Start, End, bHit ? FColor::Red : FColor::Green, false, 1, 0, 1);
	// if (bHit)
	// {
	// 	DrawDebugSphere(GetWorld(), HitResult.Location, BallMesh->Bounds.SphereRadius, 12, FColor::Red, false, 1);
	// }
}
void AMainBall::UpdateRotation(float DeltaTime)
{
	// Обновляем угловую скорость с учетом демпфирования
	AngularVelocity *= DampingCoefficient;

	// Вычисляем новый поворот на основе угловой скорости
	FRotator DeltaRotation = FRotator::MakeFromEuler(AngularVelocity * DeltaTime);

	// Применяем новый поворот к мячу
	FQuat QuatRotation = FQuat(DeltaRotation);
	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);
}

FVector AMainBall::ReflectVector(const FVector& InVector, const FVector& Normal)
{
	return InVector - 2 * FVector::DotProduct(InVector, Normal) * Normal;
}