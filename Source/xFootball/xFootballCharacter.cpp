// Copyright Epic Games, Inc. All Rights Reserved.

#include "xFootballCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Ball/BallHandlerComponent.h"
#include "Ball/MainBall.h"
#include "GAS/BasicAttributeSet.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// AxFootballCharacter

AxFootballCharacter::AxFootballCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	BallHandlerComponent = CreateDefaultSubobject<UBallHandlerComponent>(TEXT("BallHandlerComponent"));
}
void AxFootballCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	if(IsValid(AbilitySystemComponent))
	{
		BasicAttributeSet = AbilitySystemComponent->GetSet<UBasicAttributeSet>();
	}
}

void AxFootballCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(PlayerGameState == EPlayerState::ChasingBall)
	{
		if(BallHandlerComponent->ControlledBall!=nullptr)
		{
			FVector ChaseDirection = (BallHandlerComponent->ControlledBall->GetActorLocation() - GetActorLocation()).GetSafeNormal2D();
			AddMovementInput(ChaseDirection,1);
		}
	}
}

void AxFootballCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AxFootballCharacter::Move);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Canceled, this, &AxFootballCharacter::Move);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &AxFootballCharacter::Move);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AxFootballCharacter::Move(const FInputActionValue& Value)
{
	CurrentInput = Value.Get<FVector3d>();
	if(PlayerGameState == EPlayerState::None)
	{
		FVector2D MovementVector = Value.Get<FVector2D>();

		if (Controller != nullptr)
		{
			FVector Input = FVector(MovementVector.Y,MovementVector.X,0);
			AddMovementInput(FVector(MovementVector.Y,MovementVector.X,0),1);
			BallHandlerComponent->PlayerInput(Input);
		}
	}
	
}

void AxFootballCharacter::InputCanceledOrStopped()
{
	CurrentInput.Set(0,0,0);
}

void AxFootballCharacter::PossessBall(AActor* Ball) const
 {
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors, AMainBall::StaticClass());

	if (OverlappingActors.Num() > 0)
	{
		AActor* NearestBall = OverlappingActors[0];
		for (AActor* Ball : OverlappingActors)
		{
			if (FVector::Dist(Ball->GetActorLocation(), GetActorLocation()) < FVector::Dist(NearestBall->GetActorLocation(), GetActorLocation()))
			{
				NearestBall = Ball;
			}
		}
		
		BallHandlerComponent->SetBall(static_cast<AMainBall*>(NearestBall),const_cast<AxFootballCharacter*>(this));
	}
}

void AxFootballCharacter::ReleaseBall() const
{
	BallHandlerComponent->ReleaseBall();
}
