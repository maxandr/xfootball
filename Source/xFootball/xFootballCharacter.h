#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "Ball/BallHandlerComponent.h"
#include "xFootballCharacter.generated.h"

class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);
UENUM(BlueprintType)
enum class EPlayerState : uint8
{
	None,
	ChasingBall
};

UCLASS(config=Game)
class AxFootballCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

public:
	AxFootballCharacter();
	UFUNCTION(BlueprintCallable)
	void PossessBall(AActor* Ball) const;

	UFUNCTION(BlueprintCallable)
	void ReleaseBall() const;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="GAS",meta=(AllowPrivateAccess = true))
	const class UBasicAttributeSet* BasicAttributeSet;
	
	EPlayerState GetPlayerGameState() const {return PlayerGameState;};
	void SetPlayerGameState(EPlayerState NewState) { PlayerGameState = NewState;};
	friend UBallHandlerComponent;

	UPROPERTY(BlueprintReadOnly)
	FVector CurrentInput;

protected:
	virtual void Move(const FInputActionValue& Value);
	virtual void InputCanceledOrStopped();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override
	{
		return AbilitySystemComponent;
	};

	UPROPERTY(BlueprintReadOnly,EditAnywhere,Category="GAS",meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UBallHandlerComponent* BallHandlerComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "State", meta = (AllowPrivateAccess = "true"))
	EPlayerState PlayerGameState;
};

 
